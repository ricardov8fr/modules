# -*- coding: utf-8 -*-

from odoo import api, models, fields
from odoo.exceptions import UserError, ValidationError


class EmployeeContact(models.Model):
    _inherit = 'hr.employee'

    address_home_id = fields.Many2one('res.partner')
    employee_document = fields.Integer(related='address_home_id.id', string="Employee Document", groups="hr.group_hr_user")
    employee_email = fields.Char(related='address_home_id.email', string="Employee Email", groups="hr.group_hr_user")

    @api.onchange('address_home_id')
    def action_employee_address(self):
        email = self.env['hr.employee'].search([['private_email','=', self.private_email]])
        if email:
            for e in email:
                if e != self.employee_email:
                    raise UserError(("Error: Ya existe un empleado con este contacto registrado."))

# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Employee Contacts',
    'version': '1.0',
    'category': 'Human Resources',
    'description': """
        This module was created to record the employee's private contact information automatically.
    """,
    'depends': ['hr'],
    'data': [
        'views/hr_employee_contacts_views.xml',
    ],
}
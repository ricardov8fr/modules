# -*- coding: utf-8 -*-

from odoo import api, models, fields
from odoo.exceptions import UserError, ValidationError
from datetime import datetime, date
from dateutil import relativedelta


class EmployeeSons(models.Model):
    _name = 'hr.employee.sons'
    _description = 'Employee Sons'

    parent_id = fields.Many2one('hr.employee', "Parent", required=True)
    son_name = fields.Char(string='Son Name', required=True)
    son_document = fields.Integer(string='Son Document', required=True)
    studies = fields.Selection([
        ('preschool', 'Preschool'),
        ('primary', 'Primary'),
        ('secondary', 'Secondary'),
        ('professional', 'Professional'),
        ('none', 'None')], string='Studies', required=True)
    son_birthday = fields.Date(string='Son Birthday', required=True)
    son_age = fields.Integer(string='Son Age', readonly=True)

    @api.onchange('son_document')
    def action_document_sons(self):
        register = self.env['hr.employee.sons'].search([['son_document','=', self.son_document]])
        if register:
            for r in register:
                if r.id != self.son_document:
                    raise UserError(("Error: Ya existe un registro con este número de identificación."))

    @api.onchange('son_birthday')
    def _calculate_years(self):
        current_date = date.today()
        difference = relativedelta.relativedelta(current_date, self.son_birthday)
        self.son_age = difference.years

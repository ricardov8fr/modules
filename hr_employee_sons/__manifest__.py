# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Employee Sons',
    'version': '1.0',
    'category': 'Human Resources',
    'description': """
        This module was created to register the children of employees.
    """,
    'depends': ['hr'],
    'data': [
        'security/ir.model.access.csv',
        'views/hr_employee_views.xml',
    ],
}